import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

import InputField from './InputField.js';

class AddressInfo extends Component{
  render(){
    let setFieldValue = this.props.setFieldValue;
    return(
      <Row>
        <h3> Address Info </h3>
        <Col xs={12}>
          <InputField field='address' onChange={setFieldValue} />
        </Col>
        <Col xs={9}>
          <InputField field='city' onChange={setFieldValue} />
        </Col>
        <Col xs={3}>
          <InputField field='state' onChange={setFieldValue} />
        </Col>
        <Col xs={12}>
          <InputField field='county' onChange={setFieldValue} />
        </Col>
        <Col xs={12}>
          <InputField field='zip' onChange={setFieldValue} />
        </Col>
      </Row>
    )
  }
}

export default AddressInfo;
