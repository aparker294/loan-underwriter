import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';

import UnitForm from './UnitForm.js';

class RentRoll extends Component{
  render(){
    let setCurrentUnitFieldValue = this.props.setCurrentUnitFieldValue;
    return(
      <Row>
        <Col xs={12}>
          <Row>
            <Col xs={12} md={3}>
              <h3>Rent Roll</h3>
            </Col>
            <Col xs={12} md={2}>
              <Button onClick={this.props.addCurrentUnit}> Add Another Unit </Button>
            </Col>
            <Col xs={12} md={5}>
              <Row>
                <Col xs={4}>
                  <h3>Unit #</h3>
                </Col>
                <Col xs={4}>
                  <h3>Rent</h3>
                </Col>
              </Row>
            </Col>
            <Col xs={6} md={2}>
              <Button onClick={this.props.clearUnits}> Clear Units </Button>
            </Col>
          </Row>
          <Row>
            <Col xs={5}>
              <UnitForm setCurrentUnitFieldValue={setCurrentUnitFieldValue} />
            </Col>
            <Col xs={7}>
              {
                this.props.units.map((unit, i) => {
                  return  <Row key={i}>
                            <Col xs={4}>
                              { unit.number }
                            </Col>
                            <Col xs={4}>
                              { unit.rent }
                            </Col>
                          </Row>
                })
              }
            </Col>
          </Row>
        </Col>
      </Row>
    )
  }
}

export default RentRoll;
