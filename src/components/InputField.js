import React, { Component } from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

class InputField extends Component{
  render(){
    let field = this.props.field;
    let label = typeof this.props.label !== 'undefined' ? this.props.label
      : this.uppercaseFirst(field);
    return(
      <FormGroup>
        <ControlLabel>{label}</ControlLabel>
        <FormControl
            type="text"
            onChange={(event) => this.props.onChange(field, event.target.value)} />
      </FormGroup>
    )
  }

  updateFieldValue(field, value) {
    this.props.onChange(field, value);
  }

  uppercaseFirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
}

export default InputField;
