import React, { Component } from 'react';
import {Col, Row} from 'react-bootstrap';

class Response extends Component{
  render(){
    let term = this.props;

    return(
      <Row>
        <Col xs={6}>
          <label>NOI:</label> <br/>
          <label>Value:</label> <br/>
          <label>75% LTV Proceeds:</label> <br/>
          <label>Treasury:</label> <br/>
          <label>Interest Rate:</label> <br/>
          <label>Debt Constant: </label> <br/>
          <label>Annual Debt Service: </label> <br/>
          <label>Years: </label> <br/>
          <label># Payments: </label> <br/>
          <label>Payoff: </label> <br/>
          <label>Proceeds: </label> <br/>
          <label>Type: </label> <br/>
          <label>Agency: </label> <br/>
        </Col>
        <Col xs={6}>
          <div className="resultValue">{term['NOI']}</div>
          <div className="resultValue">{term['Value']}</div>
          <div className="resultValue">{term['75% LTV Proceeds']}</div>
          <div className="resultValue">{term['Treasury']}</div>
          <div className="resultValue">{term['Interest Rate']}</div>
          <div className="resultValue">{term['Debt Constant']}</div>
          <div className="resultValue">{term['Annual Debt Service']}</div>
          <div className="resultValue">{term['Years']}</div>
          <div className="resultValue">{term['# Payments']}</div>
          <div className="resultValue">{term['Payoff']}</div>
          <div className="resultValue">{term['Proceeds']}</div>
          <div className="resultValue">{term['Type']}</div>
          <div className="resultValue">{term['Agency']}</div>
        </Col>
      </Row>
    )
  }
}

export default Response;
