import { Col, Row, Button } from 'react-bootstrap';
import React, { Component } from 'react';
import FormattedResult from './FormattedResult.js';

class Results extends Component{
  render(){
    let numOfResultsToShow = this.props.numOfResultsToShow;
    return(
      <Row>
        <Col xs={12}>
          <Row>
            <Col xs={4} lg={2}>
              <h3>Results</h3> <br/>
            </Col>
            <Col id="resultButtons" xs={6}>
              <Button onClick={() => this.props.showMoreResults(numOfResultsToShow)}>Show More</Button>
              <Button onClick={this.props.clearResults}>Clear Results</Button>
            </Col>
          </Row>
          <Row>
            {
              this.props.terms.map((term, i) => {
                if (i < numOfResultsToShow) {
                   return <Col className='resultItem' xs={12} md={5} key={i}>
                            <FormattedResult
                             {...term} key={i}/>
                          </Col>;
                }
                return '';
              })
            }
          </Row>
        </Col>
      </Row>
    )
  }
}

export default Results;
