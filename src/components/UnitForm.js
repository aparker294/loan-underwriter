import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

import InputField from './InputField.js';

class UnitForm extends Component{
  render(){
    let setCurrentUnitFieldValue = this.props.setCurrentUnitFieldValue;
    return(
      <Row className='unitForm'>
        <Col xs={4}>
          <InputField field='number' label='Unit #' onChange={setCurrentUnitFieldValue} />
        </Col>
        <Col xs={4}>
          <InputField field='bedrooms' onChange={setCurrentUnitFieldValue} />
        </Col>
        <Col xs={4}>
          <InputField field='bathrooms' onChange={setCurrentUnitFieldValue} />
        </Col>
        <Col xs={4}>
          <InputField field='rent' onChange={setCurrentUnitFieldValue} />
        </Col>
        <Col xs={5}>
          <InputField field='annualTotal' label='Anual Total' onChange={setCurrentUnitFieldValue} />
        </Col>
        <Col xs={3}>
          <InputField field='vacancy' onChange={setCurrentUnitFieldValue} />
        </Col>
      </Row>
    )
  }
}

export default UnitForm;
