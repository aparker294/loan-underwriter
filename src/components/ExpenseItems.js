import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

import InputField from './InputField.js';

class ExpenseItems extends Component{
  render(){
    let setFieldValue = this.props.setFieldValue;
    return(
      <Row>
        <h3>Expense Items</h3>
        <Col xs={6}>
          <InputField field='marketing' onChange={setFieldValue} />
        </Col>
        <Col xs={6}>
          <InputField field='taxes' onChange={setFieldValue} />
        </Col>
        <Col xs={6}>
          <InputField field='insurance' onChange={setFieldValue} />
        </Col>
        <Col xs={6}>
          <InputField field='repairs' onChange={setFieldValue} />
        </Col>
        <Col xs={6}>
          <InputField field='administration' onChange={setFieldValue} />
        </Col>
        <Col xs={6}>
          <InputField field='payroll' onChange={setFieldValue} />
        </Col>
        <Col xs={6}>
        <InputField field='utility' onChange={setFieldValue} />
          </Col>
        <Col xs={6}>
          <InputField field='management' onChange={setFieldValue} />
        </Col>
      </Row>
    )
  }
}

export default ExpenseItems;
