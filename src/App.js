import './App.css';
import React, { Component } from 'react';
import { Button, Col, Row } from 'react-bootstrap';

import InputField from './components/InputField.js';
import Results from './components/Results.js';
import RentRoll from './components/RentRoll.js';
import AddressInfo from './components/AddressInfo.js';
import ExpenseItems from './components/ExpenseItems.js';

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      currentUnit: {},
      units: [],
      address: '',
      city: '',
      state: '',
      county: '',
      zip: '',
      marketing: '',
      taxes: '',
      insurance: '',
      repairs: '',
      administration: '',
      payroll: '',
      utility: '',
      management: '',
      capitalizationRate: '',
      result: false,
      results: [ 'Results'],
      numOfResultsToShow: 3
    };
  }

  render() {
    return (
      <div className="">
        <h1>Loan Underwriting</h1>
        <p>Please enter your property information below</p>
        <form onSubmit={this.onSubmit}>
          <Row>
            <Col xs={4}>
              <AddressInfo
                setFieldValue={this.setFieldValue.bind(this)} />
            </Col>
            <Col xs={4}>
              <ExpenseItems
              setFieldValue={this.setFieldValue.bind(this)} />
            </Col>
            <Col xs={4} md={3}>
              <h3>&nbsp;</h3>
              <InputField
                field='capitalizationRate'
                label='Capitalization Rate'
                onChange={this.setFieldValue.bind(this)} />
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <RentRoll
                units={this.state.units}
                clearUnits={this.clearUnits.bind(this)}
                addCurrentUnit={this.addCurrentUnit.bind(this)}
                setCurrentUnitFieldValue={this.setCurrentUnitFieldValue.bind(this)} />
            </Col>
          </Row>
          <Row id="getLoanResults">
            <Col xs={6}>
              <Button type="submit">Get Loan results</Button>
            </Col>
          </Row>
        </form>
        {
          typeof this.state.results.terms !== 'undefined' ?
            <Results
              terms = {this.state.results.terms}
              clearResults = {this.clearResults.bind(this)}
              showMoreResults = {this.showMoreResults.bind(this)}
              numOfResultsToShow = {this.state.numOfResultsToShow} />
            : ''
        }
      </div>
    );
  }

  setFieldValue(field, value) {
    this.setState({ [field]: value });
  }

  setCurrentUnitFieldValue(field, value) {
    let currUnit = Object.assign({}, this.state.currentUnit);
    currUnit[field] = value;
    this.setState({ currentUnit: currUnit });
  }

  addCurrentUnit() {
    let currUnit = Object.assign({}, this.state.currentUnit);
    this.setState(prevState => ({
        units: [...prevState.units, currUnit]
    }));
  }

  showMoreResults(showMoreResults) {
    this.setState({ numOfResultsToShow: showMoreResults + 3 });
  }

  clearUnits () {
    this.setState({ units: [] });
  }

  clearResults () {
    this.setState({ results: []});
  }

  onSubmit = (e) => {
    e.preventDefault();
    // pull out the data from the state
    let { address, city, state, county, zip, units, capitalizationRate } = this.state;

    let expense = this.getExpense();
    let income = units.reduce( function(cnt,unit){ return cnt + unit.rent; }, 0);

    let noi = income - expense;

    let data = {
      income  : income,
      expenses: expense,
      rate    : capitalizationRate,
      noi     : noi,
      address : {
       address: address,
       city  : city,
       state : state,
       county: county,
       zip   : zip
     }
    };
    this.postData(data);
  }

  getExpense() {
    let { taxes, marketing, insurance, repairs, administration, payroll, utility, management } = this.state;
    return parseInt(taxes, 10)
          + parseInt(marketing, 10)
          + parseInt(insurance, 10)
          + parseInt(repairs, 10)
          + parseInt(administration, 10)
          + parseInt(payroll, 10)
          + parseInt(utility, 10)
          + parseInt(management, 10);
  }

  postData(data) {
    fetch("https://script.google.com/macros/s/AKfycbwPGz6uQQS9IW33ASPYlcWaEtRMD8eDAK1ONg7lT2dREXpaSUYh/exec", {
      method: "POST",
      body: JSON.stringify(data)
    }).then(res => res.json())
      .then(
        (result) => {
          this.setState({
            results: result
          });
        },
        (error) => {
        }
      );
  }

}

export default App;
